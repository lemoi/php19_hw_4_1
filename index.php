<?php
/* Домашнее задание к лекции 4.1 «Реляционные базы данных и SQL»

Базовое задание:
Необходимо выбрать все данные из таблицы books (если используется сервер Нетологии, то база данных global, если нет,
то дамп базы данных находится в файле (https://netology-university.bitbucket.io/php/homework/4.1-sql/dump.txt)) и вывести их на странице в виде таблицы.
Для подключения к базе данных на сервере Нетологии, нужно использовать хост localhost, а имя пользователя и пароль совпадают с именем пользователя и паролем к FTP.

Расширенное задание:
Добавить возможность фильтровать данные по трем параметрам: ISBN, name, author. Для ввода данных для фильтрации следует использовать текстовые поля.
Фильтр должен работать по принципу поиска введенной подстроки в любом месте строки (как мы разбирали во время лекции).

Задание для самых отчаянных:
Фильтры должны суммироваться, т.е. если пользователь ввел фильтр по ISBN, который возвращает 3 записи, и при этом ввел фильтр по имени автора,
который возвращает 2 записи, то в результате будут выведены записи, которые соответствуют фильтру по ISBN и по имени автора.
При этом введенные значения фильтров должны сохраняться в полях для ввода после вывода страницы с результатами.

Пример того, как все должно работать в «максимальной комплектации» (http://university.netology.ru/u/vfilipov/homework.php).
Рекомендую делать по порядку, от простого к сложному.
*/

error_reporting(E_ALL);
ini_set('display_errors', 1);

function getParam($name) {
    return array_key_exists($name, $_GET) ? $_GET[$name] : null;
}

//$pdo = new PDO("mysql:host=localhost;charset=utf8;dbname=global", "lmoiseev", "neto1490");
$pdo = new PDO("mysql:host=localhost;port=3306;charset=utf8;dbname=global", "root", "root");

$fields = [];
$sql = "SHOW COLUMNS FROM `books`";
foreach ($pdo->query($sql) as $row) {
    $fields[] = $row['Field'];
}

$sql = "SELECT * FROM `books`";
$where = [];
foreach ($fields as $field) {
    $value = getParam($field);
    if (!empty($value)) {
        $where[] = "`$field` LIKE '%$value%'";
    }
}
if (count($where)) {
    $sql .= ' WHERE ' . implode(' AND ', $where);
}

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>PHP-19. Task 4.1</title>
    <style>
        table {
            border-spacing: 0;
            border-collapse: collapse;
            margin-top: 10px;
        }

        table td, table th {
            border: 1px solid #ccc;
            padding: 5px;
        }

        table th {
            background: #eee;
        }
    </style>
</head>
<body>

<h1>Библиотека успешного человека</h1>

<form method="GET">
    <input type="text" name="isbn" placeholder="ISBN" value="<?=htmlspecialchars(getParam('isbn'))?>"/>
    <input type="text" name="name" placeholder="Название книги" value="<?=htmlspecialchars(getParam('name'))?>"/>
    <input type="text" name="author" placeholder="Автор книги" value="<?=htmlspecialchars(getParam('author'))?>"/>
    <input type="submit" value="Поиск"/>
</form>

<table>
    <tr>
        <th>Название</th>
        <th>Автор</th>
        <th>Год выпуска</th>
        <th>Жанр</th>
        <th>ISBN</th>
    </tr>
    <?php
    foreach ($pdo->query($sql) as $row) {
        ?>
        <tr>
            <td><?=htmlspecialchars($row['name'])?></td>
            <td><?=htmlspecialchars($row['author'])?></td>
            <td><?=htmlspecialchars($row['year'])?></td>
            <td><?=htmlspecialchars($row['genre'])?></td>
            <td><?=htmlspecialchars($row['isbn'])?></td>
        </tr>
        <?php
    }
    ?>
</table>

</body>
</html>
